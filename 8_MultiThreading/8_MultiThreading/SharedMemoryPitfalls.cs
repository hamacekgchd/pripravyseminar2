﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _8_MultiThreading
{
    class SharedMemoryPitfalls
    {

        // Plan:
        // What does this do?
        
        public static void main()
        {
            SharedNumberData num = new SharedNumberData();
            num.Number = 0;
            var worker1 = new WorkerWithData(num);
            var worker2 = new WorkerWithData(num);
            Thread thread1 = new Thread(worker1.DoWork);
            Thread thread2 = new Thread(worker2.DoWork);
            thread1.Name = "1";
            thread2.Name = "2";
            thread1.Start();
            thread2.Start();
        }
    }

    class SharedNumberData
    {
        public int Number = 0;
    }

    class WorkerWithData
    {
        private SharedNumberData Data;

        public WorkerWithData(SharedNumberData data)
        {
            Data = data;
        }

        public void DoWork()
        {
            for (int i = 0; i < 1000000; i++)
            {
                Data.Number++;
            }
            Console.WriteLine(Thread.CurrentThread.Name + ": Shared data number "+Data.Number);
        }

    }
}
