﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace _8_MultiThreading
{
    /**
     * Plan: 
     * 1) Linked list trouble theory.
     * 2) Stack trouble show.
     * 3) Stack lock show.
     * 4) Concurrent stack show.
     */
    class SharedMemoryStack
    {

        public static void main()
        {
            var stack = new Stack<int>();
            var stackWorker1 = new StackWorker(stack);
            var stackWorker2 = new StackWorker(stack);
            var thread1 = new Thread(stackWorker1.DoWork);
            var thread2 = new Thread(stackWorker2.DoWork);
            thread1.Start();
            thread2.Start();
            thread1.Join();
            thread2.Join();
            Console.WriteLine("Count: " + stack.Count);
            foreach (var item in stack)
            {
                Console.Write(item);
                Console.Write(" ");
            }
        }

    }

    class StackWorker
    {
        private Stack<int> Stack;

        public StackWorker(Stack<int> stack)
        {
            this.Stack = stack;
        }

        public void DoWork()
        {
            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    Stack.Push(1);
                }
                for (int j = 0; j < 999; j++)
                {
                    Stack.Pop();
                }
            }
        }

    }
}
