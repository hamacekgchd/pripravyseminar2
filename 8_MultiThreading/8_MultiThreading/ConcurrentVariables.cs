﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _8_MultiThreading
{

    /*
     * Plan:
     * 1) What does this do?
     * 2) volatile
     * 3) How to remove last "Working"?
     * 
     */
    class ConcurrentVariables
    {

        public static void main()
        {
            var stopWorker = new StopWorker();
            var thread1 = new Thread(stopWorker.DoWork);
            thread1.Start();
            Thread.Sleep(500);
            Console.WriteLine("----- Trying to stop worker");
            stopWorker.Stop = true;
            Console.WriteLine("----- Worker stopped");
        }

    }

    class StopWorker
    {
        public bool Stop = false;

        public void DoWork()
        {
            while (!Stop)
            {
                Console.WriteLine("Working..");
            }
            Console.WriteLine("Ended..");
        }

    }
}
