﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_MultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            BasicExample.main();
            // SharedMemoryStack.main();
            // ConcurrentVariables.main();
            // SharedMemoryPitfalls.main();
        }
    }
}
