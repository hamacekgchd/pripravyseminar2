﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _8_MultiThreading
{
    // Plan:
    // 1) basic example
    // 2) race (example of code sharing OS switching contexts), what does this do?
    // 3) example of passing data
    class BasicExample
    {

        public static void main()
        {
            Thread thread = new Thread(BackgroundWork);
            thread.Name = "2";
            Thread.CurrentThread.Name = "1";
            Console.WriteLine("Starting second thread");
            thread.Start();
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("1: Cycle " + i);
            }
        }


        private static void BackgroundWork()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("2: Cycle "+i);
            }
        }

        private static void ActiveWork()
        {

            int counter = 0;
            while (counter < 1000)
            {
                for (int i = 0; i < 100; i++) ;
                counter++;
                Console.WriteLine(Thread.CurrentThread.Name + ": Cycle " + counter);
            }
        }

    }

    class WorkWithData
    {
        private int Data;

        public WorkWithData(int Data)
        {
            this.Data = Data;
        }

        public void DoWork()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(Thread.CurrentThread.Name + ": Data: " + Data + ", Cycle" + i);
            }
        }

    }
}
