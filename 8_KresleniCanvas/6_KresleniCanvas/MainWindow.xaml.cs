﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _6_KresleniCanvas
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Nastroj { Tuzka, Kruh };

        Nastroj nastroj = Nastroj.Tuzka;

        Ellipse soucasnyKruh = null;

        public MainWindow()
        {
            InitializeComponent();
        }
        

        private void Tuzka_Click(object sender, RoutedEventArgs e)
        {
            this.nastroj = Nastroj.Tuzka;
        }

        private void Kruh_Click(object sender, RoutedEventArgs e)
        {
            this.nastroj = Nastroj.Kruh;
        }

        private void MyCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(nastroj == Nastroj.Kruh)
            {
                soucasnyKruh = new Ellipse() { Fill = Brushes.Black,
                    Stroke = Brushes.Yellow, Width = 1,
                    Height = 1, StrokeThickness = 10 };
                Point mousePosition = e.GetPosition(MyCanvas);
                Canvas.SetTop(soucasnyKruh, mousePosition.Y);
                Canvas.SetLeft(soucasnyKruh, mousePosition.X);
                MyCanvas.Children.Add(soucasnyKruh);
            }
        }

        private void MyCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if(nastroj == Nastroj.Kruh && soucasnyKruh != null)
            {
                Point mousePosition = e.GetPosition(MyCanvas);
                soucasnyKruh.Width = Math.Abs(mousePosition.X - Canvas.GetLeft(soucasnyKruh));
                soucasnyKruh.Height = Math.Abs(mousePosition.Y - Canvas.GetTop(soucasnyKruh));
            }
        }

        private void MyCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (nastroj == Nastroj.Tuzka)
            {
                var el = new Ellipse() { Width = 20, Height = 20, Fill = Brushes.Black };
                Point mousePosition = e.GetPosition(MyCanvas);
                Canvas.SetTop(el, mousePosition.Y);
                Canvas.SetLeft(el, mousePosition.X);
                MyCanvas.Children.Add(el);
            }
            if(nastroj == Nastroj.Kruh && soucasnyKruh != null)
            {
                Point mousePosition = e.GetPosition(MyCanvas);
                soucasnyKruh.Width = Math.Abs(mousePosition.X - Canvas.GetLeft(soucasnyKruh));
                soucasnyKruh.Height = Math.Abs(mousePosition.Y - Canvas.GetTop(soucasnyKruh));
                soucasnyKruh = null;
            }
        }
    }
}
