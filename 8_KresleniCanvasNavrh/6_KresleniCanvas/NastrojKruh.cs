﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace _6_KresleniCanvas
{
    class NastrojKruh : AbstraktniNastroj
    {
        private Ellipse soucasnyKruh = null;

        public NastrojKruh(Canvas canvas) : base(canvas)
        {
        }

        public override void MouseDown(object source, MouseEventArgs e)
        {
            base.MouseDown(source, e);
            soucasnyKruh = new Ellipse() { Fill = Brushes.Black, Stroke = Brushes.Yellow, Width = 1, Height = 1, StrokeThickness = 10 };
            Point mousePosition = e.GetPosition(Canvas);
            Canvas.SetTop(soucasnyKruh, mousePosition.Y);
            Canvas.SetLeft(soucasnyKruh, mousePosition.X);
            Canvas.Children.Add(soucasnyKruh);
        }

        public override void MouseMove(object source, MouseEventArgs e)
        {
            base.MouseMove(source, e);
            if(soucasnyKruh != null)
            {
                Point mousePosition = e.GetPosition(Canvas);
                soucasnyKruh.Width = Math.Abs(mousePosition.X - Canvas.GetLeft(soucasnyKruh));
                soucasnyKruh.Height = Math.Abs(mousePosition.Y - Canvas.GetTop(soucasnyKruh));
            }
        }

        public override void MouseUp(object source, MouseEventArgs e)
        {
            base.MouseUp(source, e);
            if(soucasnyKruh != null)
            {
                Point mousePosition = e.GetPosition(Canvas);
                soucasnyKruh.Width = Math.Abs(mousePosition.X - Canvas.GetLeft(soucasnyKruh));
                soucasnyKruh.Height = Math.Abs(mousePosition.Y - Canvas.GetTop(soucasnyKruh));
                soucasnyKruh = null;
            }
        }
    }
}
