﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace _6_KresleniCanvas
{
    abstract class AbstraktniNastroj
    {
        public AbstraktniNastroj(Canvas canvas)
        {
            Canvas = canvas;
        }

        protected Canvas Canvas;

        public virtual void MouseDown(object source, MouseEventArgs e) { }
        public virtual void MouseMove(object source, MouseEventArgs e) { }
        public virtual void MouseUp(object source, MouseEventArgs e) { }
    }
}
