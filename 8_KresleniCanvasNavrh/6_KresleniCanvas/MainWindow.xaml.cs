﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _6_KresleniCanvas
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AbstraktniNastroj nastroj;

        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            nastroj = new NastrojTuzka(MyCanvas);
        }

        private void Tuzka_Click(object sender, RoutedEventArgs e)
        {
            nastroj = new NastrojTuzka(MyCanvas);
        }

        private void Obdelnik_Click(object sender, RoutedEventArgs e)
        {
            nastroj = new NastrojPadajiciObdelnik(MyCanvas);
        }

        private void Kruh_Click(object sender, RoutedEventArgs e)
        {
            nastroj = new NastrojKruh(MyCanvas);
        }

        private void MyCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            nastroj.MouseDown(sender, e);
        }

        private void MyCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            nastroj.MouseMove(sender, e);
        }

        private void MyCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            nastroj.MouseUp(sender, e);
        }
    }
}
