﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace _6_KresleniCanvas
{
    class NastrojTuzka : AbstraktniNastroj
    {
        public NastrojTuzka(Canvas canvas) : base(canvas)
        {
        }

        public override void MouseDown(object source, MouseEventArgs e)
        {
            base.MouseDown(source, e);
        }

        public override void MouseMove(object source, MouseEventArgs e)
        {
            base.MouseMove(source, e);
        }

        public override void MouseUp(object source, MouseEventArgs e)
        {
            base.MouseUp(source, e);
            var el = new Ellipse() { Width = 3, Height = 3, Fill = Brushes.Black };
            Point mousePosition = e.GetPosition(Canvas);
            Canvas.SetTop(el, mousePosition.Y);
            Canvas.SetLeft(el, mousePosition.X);
            Canvas.Children.Add(el);
        }


    }
}
