﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _6_KresleniCanvas
{
    class NastrojPadajiciObdelnik : AbstraktniNastroj
    {
        public NastrojPadajiciObdelnik(Canvas canvas) : base(canvas)
        {
        }

        private Rectangle current = null;

        public override void MouseUp(object source, MouseEventArgs e)
        {
            base.MouseUp(source, e);
            var rect = new Rectangle()
            {
                Fill = Brushes.Red, Width = 20, Height = 20
            };
            Point mousePos = e.GetPosition(Canvas);
            Canvas.SetTop(rect, mousePos.Y);
            Canvas.SetLeft(rect, mousePos.X);
            DispatcherTimer t = new DispatcherTimer();
            t.Tick += this.TimerTick;
            t.Interval = new TimeSpan(0, 0, 0, 0, 50);
            Canvas.Children.Add(rect);
            current = rect;
            t.Start();
        }

        protected void TimerTick(object sender, EventArgs e)
        {
            if(current != null)
            {
                Canvas.SetTop(current, Canvas.GetTop(current) + 2);
            }
        }
    }
}
