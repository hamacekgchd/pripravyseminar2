﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace _7_Vyjimky
{
    class Program
    {
        // na tomhle si vyzkousime jednoduchy try catch
        static void Pole()
        {
            try { 
                Console.WriteLine("Zacinam pole");
                int[] pole = new int[10];
                for (int i = 0; i <= 10; i++)
                {
                    Console.WriteLine(pole[i]);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Nejde to {0}", e.Message);
            }
        }

        static void SouborTryCatch()
        {
            Console.WriteLine("Zacinam TryCatch. Kam?");
            string kam = Console.ReadLine();
            StreamWriter writer = null;
            try {
                writer = new StreamWriter(kam);
                char c = (char)Console.Read();
                while(c != 'x')
                {
                    writer.Write(c);
                    c = (char)Console.Read();
                }
            }catch(DirectoryNotFoundException e)
            {
                Console.Error.WriteLine("Složka, do které mám zapisovat neexistuje. (soubor {0})", kam);
            }catch(ArgumentException e)
            {
                Console.Error.WriteLine("Neplatné jméno souboru.\nTyp chyby: {0}\nText chyby: {1}", e.GetType().Name ,e.Message);
            }finally
            {
                if (writer != null) writer.Dispose();
            }
        }

        // Ted ale zase neosetrujeme vyjimky.
        static void SouborUsing()
        {
            Console.WriteLine("Zacinam using. Kam?");
            string kam = Console.ReadLine();
            using (StreamWriter writer = new StreamWriter(kam))
            {
                char c = (char)Console.Read();
                while (c != 'x')
                {
                    writer.Write(c);
                    c = (char)Console.Read();
                }
            }
        }

        #region VlastniChyba
        // Chybi jedna vyjimka. Ktera?

        /// <summary>
        /// Nechá uživatele zadat posloupnost kladných celých čísel ukončených nulou a vypíše jejich maximum.
        /// </summary>
        /// <exception cref="InvalidNumberException">V případě, že uživatel zadal záporné číslo.</exception>
        /// <exception cref="FormatException"></exception>
        static void VlastniChyba()
        {
            Console.WriteLine("Zadej posloupnost kladnych cisel ukoncenych nulou. Vypisu ti maximum.");
            int max = int.Parse(Console.ReadLine());
            int soucasny = max;
            while(soucasny != 0)
            {
                if(soucasny < 0)
                {
                    throw new InvalidNumberException("Zadané číslo nesmí být záporné");
                }
                if(soucasny > max) { max = soucasny; }
                soucasny = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Nejvetsi bylo: {0}", max);
        }
        #endregion

        static void ZanorenaVlastniChyba()
        {
            try { 
                Smer s = new Smer();
                s.setX(1);
                s.setY(7);
            }catch(InvalidArgumentException e)
            {
                Console.WriteLine("Chybny argument");
            }
        }

        static void Main(string[] args)
        {
            //Pole();
            //SouborTryCatch();
            //SouborUsing();
            // VlastniChyba();
            ZanorenaVlastniChyba();
        }
    }

    class InvalidNumberException : ApplicationException
    {
        public InvalidNumberException(string msg) : base(msg) { }
        public InvalidNumberException(string msg, Exception e) : base(msg, e) { }
        public InvalidNumberException() : base() { }
    }

    class InvalidArgumentException : ApplicationException
    {
        public InvalidArgumentException() : base() { }
        public InvalidArgumentException(string msg) : base(msg) { }
        public InvalidArgumentException(string msg, Exception e) : base(msg, e) { }
    }

    struct Smer
    {

        private sbyte x;
        private sbyte y;
        private const sbyte limit = 2;
        
        /// <summary>
        /// Vytvori smer ze zadanych souradnic.
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <exception cref="InvalidArgumentException">Pokud je jedna ze souradnice mimo rozsah dany pomoci <see cref="limit"/></exception>
        public Smer(sbyte x, sbyte y) : this()
        {
            this.x = Check(x);
            this.y = Check(y);
        }
        
        public sbyte X
        {
            get
            {
                return x;
            }
        }
        public sbyte Y
        {
            get
            {
                return y;
            }
        }

        /// <summary>
        /// Zkontroluje a nastavi X.
        /// </summary>
        /// <param name="x"></param>
        /// <exception cref="InvalidArgumentException">Pokud je souradnice mimo rozsah dany pomoci <see cref="limit"/></exception>
        public void setX(sbyte x)
        {
            this.x = Check(x);
        }

        /// <summary>
        /// Zkontroluje a nastavi Y.
        /// </summary>
        /// <param name="y"></param>
        /// <exception cref="InvalidArgumentException">Pokud je souradnice mimo rozsah dany pomoci <see cref="limit"/></exception>
        public void setY(sbyte y)
        {
            this.y = Check(y);
        }

        /// <summary>
        /// Zkontroluje, jestli je <paramref name="cislo"/> v povolenem intervalu dle <see cref="limit"/>. Pokud ano, vraci <paramref name="cislo"/>.
        /// Jinak hazi vyjimku.
        /// </summary>
        /// <param name="cislo">Cislo ke kontrole.</param>
        /// <returns><paramref name="cislo"/></returns>
        /// <exception cref="InvalidArgumentException">Pokud je cislo mimo rozsah &lt;-<see cref="limit"/>, <see cref="limit"/>&gt;.</exception>
        private sbyte Check(sbyte cislo)
        {
            try { 
                if (cislo > limit || cislo < -limit)
                    throw new InvalidArgumentException(string.Format("Souřadnice směru nesmí být mimo rozsah <-{0}, {0}>. Nastaveno {1}.", limit, cislo));
            }
            finally
            {
                Console.WriteLine("finally");
            }
            return cislo;

        }
    }

}
