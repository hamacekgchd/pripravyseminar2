﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace _9_BackgroundWorker
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private BackgroundWorker backgroundWorker;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;
            // backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork += BackgroundWorker_DoWork;
            backgroundWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
            backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
        }

        // bezi na grafickem vlakne
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            long res = (long)e.Result;
            if (res == 0) MessageBox.Show("Výpočet skončil chybou.");
            else MessageBox.Show(string.Format("Počet menších prvočísel je {0}", res));
            Progress.Value = 0;
            // ZrusitBtn.IsEnabled = false;
        }

        // bezi na grafickem vlakne
        private void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progress.Value = e.ProgressPercentage;
        }

        // bezi na vlakne na pozadi. Zde se dela prace, ale nesmi se pristupovat do viditelnych objektu.
        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            long cislo = (long)e.Argument;
            // pocitam pocet prvocisel mensich nez zadane cislo
            long pocet = 0;
            for(long i = 2; i < cislo; i++)
            {
                if (JePrvocislo(i)) pocet++;
                backgroundWorker.ReportProgress((int)(i * 100 / cislo));
            }
            e.Result = pocet;
        }


        private void Pocitej_Click(object sender, RoutedEventArgs e)
        {
            if (!backgroundWorker.IsBusy)
            {
                backgroundWorker.RunWorkerAsync(long.Parse(Vstup.Text));
            }
        }

        private void ZrusitBtn_Click(object sender, RoutedEventArgs e)
        {
            backgroundWorker.CancelAsync();
            ZrusitBtn.IsEnabled = false;
        }

        private static bool JePrvocislo(long n)
        {
            for (long i = 2; i < n ; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
