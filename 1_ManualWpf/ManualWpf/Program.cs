﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ManualWpf
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Zacinam");
            Application app = new Application();
            app.Run(new AppWindow());
            Console.WriteLine("Koncim");
        }
    }

    class AppWindow : Window
    {

        public AppWindow()
        {
            var btn = new Button() { Content = "Tlacidlo", FontSize = 20 };
            btn.Click += Button_Click;
            var panel = new StackPanel();
            panel.Children.Add(btn);
            panel.Children.Add(new TextBlock() { Text = "Text" });
            this.Content = panel;
        }

        private void Button_Click(object sender, RoutedEventArgs args)
        {
            this.Title += "+";
        }

    }
}
